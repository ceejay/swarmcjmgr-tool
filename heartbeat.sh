#!/usr/bin/env bash
[ -z ${SERVER_API+x} ] && SERVER_API=http://101.133.169.179:7081//api/v1
[ -z ${PORT_HTTP+x} ] && PORT_HTTP=1633
[ -z ${PORT_P2P+x} ] && PORT_P2P=1634
[ -z ${PORT_DEBUG+x} ] && PORT_DEBUG=1635
[ -z ${DEBUG_API+x} ] && DEBUG_API=http://localhost

function getEthereum() {
  curl -s "$DEBUG_API:$PORT_DEBUG/addresses" | jq -r '.ethereum'
}

function getChequebook() {
  curl -s "$DEBUG_API:$PORT_DEBUG/chequebook/address" | jq -r '.chequebookAddress'
}

function getPeerCount() {
  curl -s "$DEBUG_API:$PORT_DEBUG/peers" | jq '.peers | length'
}

function getChequeTotal() {
  curl -s "$DEBUG_API:$PORT_DEBUG/chequebook/cheque" | jq -r '.lastcheques | length'
}

function getChequeAvailable() {
  curl -s "$DEBUG_API:$PORT_DEBUG/chequebook/cheque" | jq -r '.lastcheques | map(select(.lastreceived != null)) | length'
}

function getChequeUncashed() {
  local count=0
  for peer in $(getCashablePeer)
  do
    local uncashedAmount=$(getUncashedAmount $peer)
    if (( "$uncashedAmount" > 0 ))
    then
      count=$((${count} + 1))
    fi
  done
  echo $count
}

function getUncashedAmount() {
  curl -s "$DEBUG_API:$PORT_DEBUG/chequebook/cashout/$1" | jq '.uncashedAmount'
}


function getCashablePeer() {
  curl -s "$DEBUG_API:$PORT_DEBUG/chequebook/cheque" | jq -r '.lastcheques| map(select(.lastreceived!=null))  | .[].peer'
}

function heartbeat() {
  local valEthereum=$(getEthereum)
  local valChequebook=$(getChequebook)
  local valChequeAvailable=$(getChequeAvailable)
  local valChequeTotal=$(getChequeTotal)
  local valChequeUncashed=$(getChequeUncashed)
  local valPeerCount=$(getPeerCount)
  
  local data="{ \"chequebook\": \"$valChequebook\", \"ethereum\": \"$valEthereum\", \"chequeAvailable\": \"$valChequeAvailable\", \"chequeTotal\": \"$valChequeTotal\", \"chequeUncashed\": \"$valChequeUncashed\", \"peers\": \"$valPeerCount\", \"portDebug\": \"$PORT_DEBUG\", \"portHttp\": \"$PORT_HTTP\", \"portP2P\": \"$PORT_P2P\" }"
  
  local res=$(curl -s -X POST "$SERVER_API/node/heartbeat" -H "accept: */*" -H "Content-Type: application/json" -d "$data")
  echo response: $res
  
}

heartbeat

